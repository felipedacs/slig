<?php

namespace app\controllers;

// identifica a trait
use app\traits\View;

class Controller
{
  // utiliza a trait View
  use View;
}
